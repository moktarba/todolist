export class List {
   text: string;
   date : Date;
   priority : string;
   estCoche : boolean;

  constructor(data : any){
    this.text = data.text;
    this.date = data.date;
    this.priority = data.priority;
    this.estCoche = data.estCoche;
  }
}
