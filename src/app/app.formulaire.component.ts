import { Component } from '@angular/core';
import {List} from './models/list';
import {Lists} from './models/lists';
@Component({
  selector : 'app-formulaire',
  templateUrl : './app.formulaire.component.html',
})

export class AppFormulaireComponent {
   todos : any[] = Lists;
  ajouterList(text:string, date:Date, priority:number){
    let nouveaulist = {text , date, priority };
    this.todos.push(nouveaulist);
  }
}
