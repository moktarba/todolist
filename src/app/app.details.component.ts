import { Component, Input } from '@angular/core';
import { List } from './models/list';
@Component({
  selector: 'todo-details',
  templateUrl: './app.details.component.html',

})
export class AppDetailsComponent {
@Input() todoInput: List;
  edit : boolean = false;
  textbutton : string = "modifier";

  modifierTodo(bool :boolean){
    if (bool){
      this.edit = false;
      console.log(this.edit);
      console.log(bool);
      this.textbutton = "modifier";
    }
    else{
      this.edit = true;
      console.log(this.edit);
      this.textbutton = "sauvegarder";

    }
  }
}
