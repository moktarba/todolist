import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppFormulaireComponent } from './app.formulaire.component';
import { AppDetailsComponent } from './app.details.component';

@NgModule({
  declarations: [
    AppComponent, AppFormulaireComponent, AppDetailsComponent
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
