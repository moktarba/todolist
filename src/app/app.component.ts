import { Component } from '@angular/core';
import { AppFormulaireComponent } from './app.formulaire.component';
import { AppDetailsComponent } from './app.details.component';
import { List } from './models/list';
import { Lists } from './models/lists';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css',
    './app.formulaire.component.css',

],
})

export class AppComponent {
  todos : List[] = Lists;
  mypriority : string  = "basse" ;
  maTodo : List;
  estCoche : boolean = false;

  setClasses( todo : List ){
    let classes  = {
      faible : todo.priority = "basse",
      moyen : todo.priority = "moyenne",
      fort : todo.priority = "haute",
    }
    return classes;
  }
  afficheTodo(todo){
    this.maTodo = todo;
    console.log(this.maTodo);
  }
  todoCoche (index) {
    console.log(index);
    this.todos[index].estCoche = this.todos[index].estCoche ? false: true;
  }
  removeTodo(i) {
    this.todos.splice(i, 1);
  }
}
